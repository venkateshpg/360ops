import { Component, OnInit } from "@angular/core";
import { Chart } from "angular-highcharts";
import { DashboardService } from "../service/dashboard.service";
import { StagingAnalytics, CheckBoxFilter } from "../../app.models";
import { Daterangepicker, DaterangepickerConfig } from "ng2-daterangepicker";
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import { IMultiSelectOption, IMultiSelectTexts, IMultiSelectSettings } from 'angular-2-dropdown-multiselect';
import { SecurityService } from "../../app.services";
import * as moment from "moment";
import * as html2canvas from "html2canvas";
import * as jsPDF from 'jspdf';

@Component({
  selector: "app-performance",
  templateUrl: "./performance.component.html",
  styleUrls: ["./performance.component.css"],
  providers: [DashboardService]
})
export class PerformanceComponent implements OnInit {
  optionsModel: Array<any>;
  checkBoxFilter: CheckBoxFilter;
  myOptions: IMultiSelectOption[];
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    searchEmptyResult: 'Nothing found...',
    searchNoRenderText: 'Type in search box to see results...',
    defaultTitle: 'Select',
    allSelected: 'All selected',
  };
  mySettings: IMultiSelectSettings = {
    dynamicTitleMaxItems: 1,
    displayAllSelectedText: true,
  };
  bgImage: string;
  chart: Chart;
  singleDate: any;
  eventLog: any;
  time: Date;
  stagingAnalytics: StagingAnalytics;
  totalDays: number;
  public dateInputs: any = {
    start: moment().subtract(3, "month"),
    end: moment().subtract(0, "month")
  };
  public mainInput = {
    start: moment().subtract(12, "month"),
    end: moment().subtract(6, "month")
  };
  constructor(
    private dashboardService: DashboardService,
    private daterangepickerOptions: DaterangepickerConfig,
    private securityService: SecurityService
  ) {
    this.checkBoxFilter = new CheckBoxFilter;
    this.bgImage = "../../assets/images/map.jpg";
    this.daterangepickerOptions.settings = {
      locale: { format: "YYYY-MM-DD" },
      alwaysShowCalendars: false,
      ranges: {
        "Yesterday": [moment().subtract(1, "day"), moment()],
        "Last 7 days": [moment().subtract(7, "day"), moment()],
        "Last 30 days": [moment().subtract(30, "day"), moment()],
        "Previous Month": [moment().subtract(1, "month"), moment()],
        "Current Month": [moment().startOf('month'), moment()]
      }
    };

    this.singleDate = Date.now();
  }

  ngOnInit() {
    this.optionsModel = [];
    this.stagingAnalytics = new StagingAnalytics();
    //this.performance();
    this.dashboardService
      .dataPublishers(this.securityService.userData.roleId)
      .subscribe(data => {
        this.myOptions = data;
        this.performance();
      });
  }
  // Capture Selected Date
  private selectedDate(value: any, dateInput: any) {
    dateInput.start = value.start;
    dateInput.end = value.end;
    var start = moment(dateInput.start.format('YYYY-MM-DD'));
    var end = moment(dateInput.end.format('YYYY-MM-DD'));
    this.totalDays = end.diff(start, 'days');
    this.performance();
  }

  private singleSelect(value: any) {
    this.singleDate = value.start;
  }

  private applyDate(value: any, dateInput: any) {
    dateInput.start = value.start;
    dateInput.end = value.end;
  }

  public calendarEventsHandler(e: any) {
    this.eventLog += "\nEvent Fired: " + e.event.type;
  }

  public performance() {
    let startDate = this.dateInputs.start.format("YYYYMMDD");
    let endDate = this.dateInputs.end.format("YYYYMMDD");
    var start = moment(this.dateInputs.start.format('YYYY-MM-DD'));
    var end = moment(this.dateInputs.end.format('YYYY-MM-DD'));
    this.totalDays = end.diff(start, 'days');
    this.dashboardService
      .stagingAnalytics(startDate, endDate, this.optionsModel)
      .subscribe(data => {

        this.stagingAnalytics.faceBookCtr = data.filter(function (el) {
          return el["StagingDataPublishers.dataSourceName"] === "facebook";
        }).map(item => item['clickThroughRate']).reduce((a, b) => a + b, 0);

        this.stagingAnalytics.date = data.map(item => {
          let mom = moment(item.date, "YYYY-MM-DD");
          return mom.format("DD MMM YYYY");
        });

        this.stagingAnalytics.clickThroughRate = data.map(item =>
          parseFloat(item.clickThroughRate)
        );

        this.stagingAnalytics.viewability = data.map(item => {
          var parsePercent = parseFloat(item.viewability);
          var decimal = parsePercent / 100
          return decimal;
        });

        this.stagingAnalytics.bounceRate = data.map(item =>
          parseFloat(item.bounceRate)
        );

        this.stagingAnalytics.avgTimeOnPage = data.map(item =>
          parseFloat(item.avgTimeOnPage)
        );

        this.stagingAnalytics.pageViews = data.map(item =>
          parseFloat(item.pageViews)
        );

        this.stagingAnalytics.pagesSession = data.map(item =>
          parseFloat(item.pagesSession)
        );

        var totalViewability = this.stagingAnalytics.viewability.reduce(
          (a, b) => a + b,
          0
        );
        this.stagingAnalytics.overAllViewAbility = +((totalViewability / (this.totalDays + 1) * 100)).toFixed(2);
        this.splineChart();
      });
  }

  private splineChart() {
    this.chart = new Chart({
      title: {
        text: ""
      },
      plotOptions: {
        series: {
          allowPointSelect: true
        }
      },
      xAxis: {
        categories: this.stagingAnalytics.date
      },
      series: [
        {
          type: "spline",
          name: "CTR",
          data: this.checkBoxFilter.ctrCheckBox ? this.stagingAnalytics.clickThroughRate : ""
        },
        {
          type: "spline",
          name: "viewability",
          data: this.checkBoxFilter.viewabilityCheckBox ? this.stagingAnalytics.viewability : ""
        },
        {
          type: "spline",
          name: "timespent",
          data: this.checkBoxFilter.timespentCheckBox ? this.stagingAnalytics.avgTimeOnPage : ""
        },
        {
          type: "spline",
          name: "impressions per page view",
          data: this.checkBoxFilter.impressionsCheckBox ? this.stagingAnalytics.pageViews : ""
        },
        {
          type: "spline",
          name: "pageviews per session",
          data: this.checkBoxFilter.pageviewsCheckBox ? this.stagingAnalytics.pagesSession : ""
        },
        {
          type: "spline",
          name: "bounce rate",
          data: this.checkBoxFilter.bouncerateCheckBox ? this.stagingAnalytics.bounceRate : ""
        }
      ]
    });
  }
  public download() {
    html2canvas(document.getElementById('performanceData'), { width: 2000, height: 1000, allowTaint: true }).then(function (canvas) {
      var ctx = canvas.getContext('2d');
      var doc = new jsPDF("l", "mm", "a4");
      var img = canvas.toDataURL("image/png");
      doc.addImage(img, 'JPEG', 10, 10, 450, 150);
      doc.addPage();
      doc.save('PerformanceAnalytics.pdf');
    });
  }
}
