import { Component, OnInit, ViewChild } from "@angular/core";
import { Chart, Highcharts, HIGHCHARTS_MODULES } from "angular-highcharts";
import { DashboardService } from "../service/dashboard.service";
import { StagingAnalytics, DataPublishers, CheckBoxFilter } from "../../app.models";
import { Daterangepicker, DaterangepickerConfig } from "ng2-daterangepicker";
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import { IMultiSelectOption, IMultiSelectTexts, IMultiSelectSettings } from 'angular-2-dropdown-multiselect';
import { SecurityService } from "../../app.services";
import * as moment from "moment";
import * as jsPDF from 'jspdf';
import * as html2canvas from "html2canvas";

@Component({
  selector: "app-finance",
  templateUrl: "./finance.component.html",
  styleUrls: ["./finance.component.css"],
  providers: [DashboardService]
})
export class FinanceComponent implements OnInit {
  optionsModel: Array<any>;
  myOptions: IMultiSelectOption[];
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    searchEmptyResult: 'Nothing found...',
    searchNoRenderText: 'Type in search box to see results...',
    defaultTitle: 'Select',
    allSelected: 'All selected',
  };
  mySettings: IMultiSelectSettings = {
    
  };
  @ViewChild("pieChart") pieChart;
  @ViewChild("graph") graph;
  context: CanvasRenderingContext2D;
  bgImage: string;
  combinationChart: Chart;
  dataSource: Chart;
  singleDate: any;
  eventLog: any;
  checkBoxFilter: CheckBoxFilter;
  stagingAnalytics: StagingAnalytics;
  totalEarnings: number;
  totalEcpm: number;
  totalRequests: number;
  totalImpression: number;
  dataPublishers: Array<any>;
  public dateInputs: any = {
    start: moment().subtract(3, "month"),
    end: moment().subtract(0, "month")
  };
  public mainInput = {
    start: moment().subtract(12, "month"),
    end: moment().subtract(6, "month")
  };
  constructor(
    private dashboardService: DashboardService,
    private daterangepickerOptions: DaterangepickerConfig,
    private securityService: SecurityService
  ) {
    this.checkBoxFilter = new CheckBoxFilter;
    this.bgImage = "./assets/images/map.jpg";
    this.daterangepickerOptions.settings = {
      locale: { format: "YYYY-MM-DD" },
      alwaysShowCalendars: false,
      ranges: {
        "Yesterday": [moment().subtract(1, "day"), moment()],
        "Last 7 days": [moment().subtract(7, "day"), moment()],
        "Last 30 days": [moment().subtract(30, "day"), moment()],
        "Previous Month": [moment().subtract(1, "month"), moment()],
        "Current Month": [moment().startOf('month'), moment()]
      }
    };

    this.singleDate = Date.now();
  }

  ngOnInit() {
    this.optionsModel = [];
    this.stagingAnalytics = new StagingAnalytics();
    this.dataPublishers = new Array();
    this.dashboardService
      .dataPublishers(this.securityService.userData.roleId)
      .subscribe(data => {
        this.myOptions = data;
        this.finance();
      });

  }

  //Capture Selected Date
  private selectedDate(value: any, dateInput: any) {
    dateInput.start = value.start;
    dateInput.end = value.end;
    this.finance();
  }

  private singleSelect(value: any) {
    this.singleDate = value.start;
  }

  private applyDate(value: any, dateInput: any) {
    dateInput.start = value.start;
    dateInput.end = value.end;
  }

  public calendarEventsHandler(e: any) {
    this.eventLog += "\nEvent Fired: " + e.event.type;
  }

  public finance() {
    let startDate = this.dateInputs.start.format("YYYYMMDD");
    let endDate = this.dateInputs.end.format("YYYYMMDD");
    this.dashboardService
      .earningAnalytics(startDate, endDate, this.optionsModel)
      .subscribe(data => {
        this.stagingAnalytics.date = data.map(item => {
          let mom = moment(item.date, "YYYY-MM-DD");
          return mom.format("DD MMM YYYY");
        });
        this.stagingAnalytics.revenue = data.map(item =>
          parseFloat(item.revenue)
        );
        //Assigning CheckBox Data
        this.stagingAnalytics.ecpm = data.map(item => parseFloat(item.ecpm));
        this.stagingAnalytics.requests = data.map(item =>
          parseFloat(item.requests)
        );
        this.stagingAnalytics.fillRate = data.map(item =>
          parseFloat(item.fillRate)
        );
        this.stagingAnalytics.revenuePerImpressions = data.map(item =>
          parseFloat(item.revenuePerImpressions.replace(/,/g, ""))
        );
        this.stagingAnalytics.impressions = data.map(item =>
          parseFloat(item.impressions.replace(/,/g, ""))
        );

        this.totalEarnings = this.stagingAnalytics.revenue.reduce(
          (a, b) => a + b,
          0
        );
        this.totalEcpm = this.stagingAnalytics.ecpm.reduce(
          (a, b) => parseFloat(a) + parseFloat(b),
          0
        );
        this.totalRequests = this.stagingAnalytics.requests.reduce(
          (a, b) => a + b,
          0
        );
        this.totalImpression = this.stagingAnalytics.impressions.reduce(
          (a, b) => parseFloat(a) + parseFloat(b),
          0
        );
        this.earningsChart();
      });

    //Calling Service For Total Earnings For Pie Chart
    this.dashboardService.totalEarnings(startDate, endDate).subscribe(total => {
      const dataPublishers = [];
      this.dataPublishers = total.map(item => {
        if (item.revenue != null) {
          dataPublishers.push({
            name: item["StagingDataPublishers.dataSourceName"],
            y: parseFloat(item["revenue"])
          });
        }
      });
      this.dataPublishers = dataPublishers;
      this.dataSourceChart();
    });
  }
  //Combination of bar and line chart
  private earningsChart() {
    this.combinationChart = new Chart({
      title: {
        text: ""
      },
      xAxis: {
        categories: this.stagingAnalytics.date
      },
      labels: {
        items: [
          {
            html: "",
            style: {
              left: "50px",
              top: "18px"
            }
          }
        ]
      },
      series: [
        {
          type: "column",
          name: "earnings",
          data: this.stagingAnalytics.revenue
        },
        {
          type: "spline",
          name: "ecpm",
          data: this.checkBoxFilter.ecpmCheckBox ? this.stagingAnalytics.ecpm : ""
        },
        {
          type: "spline",
          name: "requests",
          data: this.checkBoxFilter.requestsCheckBox ? this.stagingAnalytics.requests : ""
        },
        {
          type: "spline",
          name: "fillrate",
          data: this.checkBoxFilter.fillrateCheckBox ? this.stagingAnalytics.fillRate : ""
        },
        {
          type: "spline",
          name: "impression",
          data: this.checkBoxFilter.impressionCheckBox ? this.stagingAnalytics.impressions : ""
        }
      ]
    });
  }
  //Pie Chart
  private dataSourceChart() {

    // Build the chart
    this.dataSource = new Chart({
      chart: {
        renderTo: 'chartExchange',
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: "pie"
      },
      title: {
        text: "<h4>Total earnings by data source</h4>"
      },
      plotOptions: {
        pie: {
          //allowPointSelect: true,
          cursor: "pointer",
          size: 200,

          dataLabels: {
            enabled: true,
            formatter: function () {
              return Math.round(this.percentage * 100) / 100 + " %";
            },
            distance: -30
          }
        }
      },
      series: [
        {
          name: "Brands",
          data: this.dataPublishers
        }
      ]
    });
  }

  public download() {
    html2canvas(document.getElementById('financeData'), { width: 2000, height: 1000, allowTaint: true }).then(function (canvas) {
      var ctx = canvas.getContext('2d');
      var doc = new jsPDF("l", "mm", "a4");
      var img = canvas.toDataURL("image/png");
      doc.addImage(img, 'JPEG', 10, 10, 450, 150);
      doc.addPage();
      doc.save('FinanceAnalytics.pdf');
    });
  }
}
