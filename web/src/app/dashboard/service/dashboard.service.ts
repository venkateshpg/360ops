import { ViewContainerRef, Injectable } from "@angular/core";
import { HttpModule, Http, RequestOptions } from "@angular/http";
import "rxjs/add/operator/map";
import { apiConfig } from "../../../config/global";
import "rxjs/add/operator/catch";
import { HttpClient } from "../../app.services";
@Injectable()
export class DashboardService {
  constructor(private http: Http, private httpClient: HttpClient) { }
  public getAudience(startDate, endDate) {
    return this.httpClient
      .get(apiConfig.apiUri + "reports/" + startDate + "/" + endDate)
      .map(data => data.json());
  }
  public stagingAnalytics(startDate, endDate, datasource) {
    return this.httpClient
      .post(apiConfig.apiUri + "reports/staginganalytics/", {
        startDate: startDate,
        endDate: endDate,
        datasource: datasource.length > 0 ? datasource.join("/") : []
      })
      .map(data => data.json());
  }

  public earningAnalytics(startDate, endDate, datasource) {
    return this.httpClient
      .post(apiConfig.apiUri + "reports/earninganalytics/", {
        startDate: startDate,
        endDate: endDate,
        datasource: datasource.length > 0 ? datasource.join("/") : []
      })
      .map(data => data.json());
  }
  
  public dataPublishers(role) {
    return this.httpClient
      .get(apiConfig.apiUri + "reports/dataPublishers/" + role)
      .map(data => data.json());
  }

  public totalEarnings(startDate, endDate) {
    return this.httpClient
      .post(apiConfig.apiUri + "reports/totalearnings/", {
        startDate: startDate,
        endDate: endDate,
      })
      .map(data => data.json());
  }
}
