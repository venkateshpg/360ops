import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Chart } from "angular-highcharts";
import { DashboardService } from "../service/dashboard.service";
import { Audience, CheckBoxFilter } from "../../app.models";
import { Daterangepicker, DaterangepickerConfig } from "ng2-daterangepicker";
import * as moment from "moment";
import * as jsPDF from 'jspdf';
import * as html2canvas from "html2canvas";

@Component({
  selector: "app-audience",
  templateUrl: "./audience.component.html",
  styleUrls: ["./audience.component.css"],
  providers: [DashboardService]
})
export class AudienceComponent implements OnInit {
  bgImage: string;
  chart: Chart;
  singleDate: any;
  eventLog: any;
  audience: Audience;
  checkBoxFilter: CheckBoxFilter;
  time: Date;
  totalDays: number;
  public dateInputs: any = {
    start: moment().subtract(3, "month"),
    end: moment().subtract(0, "month")
  };
  public mainInput = {
    start: moment().subtract(12, "month"),
    end: moment().subtract(6, "month")
  };
  constructor(
    private dashboardService: DashboardService,
    private router: Router,
    private daterangepickerOptions: DaterangepickerConfig
  ) {
    this.checkBoxFilter = new CheckBoxFilter;
    this.bgImage = "../../assets/images/map.jpg";
    this.daterangepickerOptions.settings = {
      locale: { format: "YYYY-MM-DD" },
      alwaysShowCalendars: false,
      ranges: {
        "Yesterday": [moment().subtract(1, "day"), moment()],
        "Last 7 days": [moment().subtract(7, "day"), moment()],
        "Last 30 days": [moment().subtract(30, "day"), moment()],
        "Previous Month": [moment().subtract(1, "month"), moment()],
        "Current Month": [moment().startOf('month'), moment()]
      }
    };

    this.singleDate = Date.now();
  }

  ngOnInit() {
    this.audience = new Audience();
    this.audienceData();
  }

  // Capture Selected Date
  private selectedDate(value: any, dateInput: any) {

    dateInput.start = value.start;
    dateInput.end = value.end;
    var start = moment(dateInput.start.format('YYYY-MM-DD'));
    var end = moment(dateInput.end.format('YYYY-MM-DD'));
    this.totalDays = end.diff(start, 'days');
    this.audienceData();
  }

  private singleSelect(value: any) {
    this.singleDate = value.start;
  }

  private applyDate(value: any, dateInput: any) {
    dateInput.start = value.start;
    dateInput.end = value.end;
  }

  public calendarEventsHandler(e: any) {
    this.eventLog += "\nEvent Fired: " + e.event.type;
  }

  public audienceData() {
    this.audience = new Audience();
    let startDate = this.dateInputs.start.format("YYYYMMDD");
    let endDate = this.dateInputs.end.format("YYYYMMDD");
    var start = moment(this.dateInputs.start.format('YYYY-MM-DD'));
    var end = moment(this.dateInputs.end.format('YYYY-MM-DD'));
    this.totalDays = end.diff(start, 'days');

    this.dashboardService.getAudience(startDate, endDate).subscribe(data => {
      this.audience.date = data.map(item => {
        let mom = moment(item.date, "YYYY-MM-DD");
        return mom.format("YYYY-MMM-DD");
      });
      this.audience.pageViews = data.map(item => {
        let s = item.pageViews;
        return parseInt(s);
      });
      // Vists
      this.audience.users = data.map(item => {
        let s = item.users;
        return parseInt(s);
      });
      
      this.audience.sessions = data.map(item => {
        let s = item.sessions;
        return parseInt(s);
      });

      const avgTimeOnPage = data.map(item => {
        return item.avgTimeOnPage;
      });

      

      var bounceRate = data.map(item =>
        parseFloat(item.bounceRate)
      );

      var totalBounceRate = bounceRate.reduce(
        (a, b) => a + b,
        0
      );
      this.audience.visits = this.audience.pageViews.reduce(
        (a, b) => a + b,
        0
      );
      this.audience.uniqueVisitors = this.audience.users.reduce(
        (a, b) => a + b,
        0
      );
      this.audience.bounceRate = ((totalBounceRate / (this.totalDays + 1) * 100)).toFixed(2);
      this.audience.avgTimeOnPage = this.secsToTime(
        this.sumDurations(avgTimeOnPage)
      );
      this.areaChart();
    });
  }

  private areaChart() {
    this.chart = new Chart({
      chart: {
        type: "areaspline"
      },
      title: {
        text: ""
      },
      credits: {
        enabled: false
      },
      xAxis: {
        categories: this.audience.date
      },
      series: [
        {
          name: "PageViews",
          data: this.checkBoxFilter.pageViewCheckBox ? this.audience.pageViews : ""
        },
        {
          name: "Visits",

          data: this.checkBoxFilter.visitsCheckBox ? this.audience.sessions : ""
        },
        {
          name: "UniqueVisits",
          data: this.checkBoxFilter.uniquevisitsCheckBox ? this.audience.users : ""
        }
      ]
    });
  }

  public sumDurations(durations) {
    return durations.reduce((sum, string) => {
      let mins, secs;
      [mins, secs] = string
        .split(":")
        .slice(-2)
        .map(n => parseInt(n, 10));
      return sum + mins * 60 + secs;
    }, 0);
  }

  public secsToTime(secs) {
    let time = new Date();
    time.setHours((secs / 3600) % 24);
    time.setMinutes((secs / 60) % 60);
    time.setSeconds(secs % 60);
    return time.toTimeString().split(" ")[0];
  }

  public download() {
    html2canvas(document.getElementById('audienceData'), { width: 2000, height: 1000, allowTaint: true }).then(function (canvas) {
      var ctx = canvas.getContext('2d');
      var doc = new jsPDF("l", "mm", "a4");
      var img = canvas.toDataURL("image/png");
      doc.addImage(img, 'JPEG', 10, 10, 450, 150);
      doc.addPage();
      doc.save('AudienceAnalytics.pdf');
    });
  }
}
