import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  RouteGuardService
} from './app.services';

import {
  DashboardComponent,
  AudienceComponent,
  PerformanceComponent,
  FinanceComponent,
  LoginComponent,
  RegisterComponent,
  ForgotComponent,
} from "./app.component";

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: "/login",
    pathMatch: 'full'
  },
  { path: 'login', component: LoginComponent },
  {
    path: "dashboard",
    component: DashboardComponent,
    children: [
      { path: "finance", component: FinanceComponent, canActivate: [RouteGuardService] },
      { path: "performance", component: PerformanceComponent, canActivate: [RouteGuardService] },
      { path: "audience", component: AudienceComponent, canActivate: [RouteGuardService] }
    ]
  }
  , { path: "register", component: RegisterComponent }
  , { path: "forgot", component: ForgotComponent }

];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes, {
  useHash: false
});
