import { Injectable } from '@angular/core';

@Injectable()
export class GlobalService {

  public getParam(name) {
    return sessionStorage.getItem(name);
  }

  public saveParam(name, value) {
    sessionStorage.setItem(name, value);
  }
}
