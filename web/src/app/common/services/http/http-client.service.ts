import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Login } from "../../../app.models";
@Injectable()
export class HttpClient {
      userData: Login
      constructor(private http: Http) {
            this.userData = new Login;
      }

      createAuthorizationHeader(headers: Headers) {
            this.userData = JSON.parse(sessionStorage.getItem('id_token'));
            headers.append('Authorization', this.userData.token);
      }

      get(url) {
            let headers = new Headers();
            this.createAuthorizationHeader(headers);
            return this.http.get(url, {
                  headers: headers
            });
      }

      post(url, data) {
            let headers = new Headers();
            this.createAuthorizationHeader(headers);
            return this.http.post(url, data, {
                  headers: headers
            });
      }
}