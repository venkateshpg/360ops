import { Injectable, ViewChild, Component } from "@angular/core";
import { HttpModule, Http, RequestOptions } from "@angular/http";
import "rxjs/add/operator/map";
import { apiConfig } from "../../../../config/global";
import "rxjs/add/operator/catch";
import { Router, Params, ActivatedRoute } from "@angular/router";
import { Login } from "../../../app.models";
import { Observable } from "rxjs";

@Injectable()
export class SecurityService {
  userData: Login;
  constructor(
    private router: Router,
    private http: Http,
  ) {
    this.userData = JSON.parse(sessionStorage.getItem("id_token")) !== null ? JSON.parse(sessionStorage.getItem("id_token")) : new Login();
  }

  public handleAuthentication() { }


  public login(username, password) {
    return this.http
      .post(apiConfig.apiUri + "user/login/", {
        userName: username,
        passwordHash: password
      })
      .map(data => data.json())
      .catch((error: any) => {
        let body = JSON.parse(error._body).message;
        return Observable.throw(new Error(error));
      });
  }

  public logout() {
    sessionStorage.clear();
    this.userData = new Login();
    this.router.navigate(["/login"]);
  }

  public setUser(Result) {
    sessionStorage.setItem("id_token", JSON.stringify(Result));
  }
}
