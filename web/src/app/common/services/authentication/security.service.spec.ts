import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MockBackend } from '@angular/http/testing';
import { HttpModule, Http, BaseRequestOptions } from '@angular/http';
import {
  SecurityService
} from './security.service';

describe('Recruit Sync Security Service', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SecurityService,
        BaseRequestOptions,
        MockBackend,
        {
          provide: Http,
          useFactory: (mockBackend: MockBackend, defaultOptions: BaseRequestOptions) => {
            return new Http(mockBackend, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        }
      ],
      imports: [RouterTestingModule]
    });
  });

  xit('should initialize security service', async(() => {

  }));


});
