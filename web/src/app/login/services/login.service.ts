import { ViewContainerRef, Injectable } from "@angular/core";
import { HttpModule, Http, RequestOptions } from "@angular/http";
import "rxjs/add/operator/map";
import { apiConfig } from "../../../config/global";
import "rxjs/add/operator/catch";
import { Observable } from "rxjs";

@Injectable()
export class LoginService {
  constructor(private http: Http) { }
  public forgot(username) {
    return this.http
      .get(apiConfig.apiUri + "user/forgot/" + username)
      .map(data => data.json());
  }
  public register(user) {
    return this.http
      .post(apiConfig.apiUri + "user", {
        user: user
      })
      .map(response => response.json());
  }
}
