import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Chart } from "angular-highcharts";
import { LoginService } from "../services/login.service";
import { Login } from "../../app.models";
import * as moment from "moment";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"],
  providers: [LoginService]
})
export class RegisterComponent implements OnInit {
  login: Login;
  error: boolean;
  time: Date;
  errMessage: string;

  constructor(
    private loginService: LoginService,
    private router: Router,
  ) {
    this.login = new Login();
    this.error = false;
    //this.errMessage='';
  }

  ngOnInit() {


  }
  public registerUser() {
    //console.log(this.login.email, this.login.password);
    this
    .loginService
    .register(this.login)
      .subscribe(data => {
          this.router.navigate(["/login"]);
      })
  }

}
