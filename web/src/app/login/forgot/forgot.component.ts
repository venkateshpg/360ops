import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Chart } from "angular-highcharts";
import { LoginService } from "../services/login.service";
import { Login } from "../../app.models";
import * as moment from "moment";
import {
  SecurityService
} from '../../app.services';

@Component({
  selector: "app-forgot",
  templateUrl: "./forgot.component.html",
  styleUrls: ["./forgot.component.css"],
  providers: [LoginService,SecurityService]
})
export class ForgotComponent implements OnInit {
  login: Login;
  error: boolean;
  time: Date;
  errMessage: string;

  constructor(
    private loginService: LoginService,
    private router: Router,
    private securityService:SecurityService
  ) {
    this.login = new Login();
    this.error = false;
    //this.errMessage='';
  }

  ngOnInit() {


  }
  public getuser() {
    this.loginService.forgot(this.login.userName)
      .subscribe(data => {
        if (data !== null) {
          this.router.navigate(["/login"]);
        } else {
          this.error=true;
          this.errMessage = "Email does not exist";

        }
      })

  }
  //Capture Selected Date


}
