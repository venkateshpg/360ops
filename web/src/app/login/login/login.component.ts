import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { Router } from "@angular/router";
import { Chart } from "angular-highcharts";
import { LoginService } from "../services/login.service";
import { Login } from "../../app.models";
import * as moment from "moment";
import {
  SecurityService
} from '../../app.services';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {
  login: Login;
  error: boolean;
  time: Date;
  errMessage: string;

  constructor(
    private loginService: LoginService,
    private router: Router,
    private securityService: SecurityService,
  ) {
    this.login = new Login();
    this.error = false;
  }

  ngOnInit() {
  }
  public loginuser() {
    this.securityService.login(this.login.userName, this.login.passwordHash)
      .subscribe(data => {
        if (data !== null) {
          this.securityService.userData = data;
          let userdata = this.securityService.setUser(data);
          if (this.securityService.userData.roleId === 1) {
            this.router.navigate(["/dashboard/audience"]);
          } else if (this.securityService.userData.roleId === 2) {
            this.router.navigate(["/dashboard/finance"]);
          } else if (this.securityService.userData.roleId === 3) {
            this.router.navigate(["/dashboard/performance"]);
          }
        } else {
          this.error = true;
          this.errMessage = "Email and Password does not match";
        }
      })

  }
  //Capture Selected Date
}
