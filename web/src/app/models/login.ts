export class Login {
  id:number;
  userName:string;
  passwordHash:string;
  firstName:string;
  lastName:string;
  companyId:number;
  isActive:number;
  roleId:number;
  token: string;
}
