export class CheckBoxFilter {
      pageViewCheckBox: boolean = true;
      uniquevisitsCheckBox: boolean;
      visitsCheckBox: boolean;
      ecpmCheckBox: boolean = true;
      requestsCheckBox: boolean = true;
      fillrateCheckBox: boolean;
      impressionCheckBox: boolean;
      ctrCheckBox: boolean = true;
      viewabilityCheckBox: boolean = true;
      timespentCheckBox: boolean;
      impressionsCheckBox: boolean;
      pageviewsCheckBox: boolean;
      bouncerateCheckBox: boolean;
}