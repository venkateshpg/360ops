export class Audience {
  dataSource: number;
  date: any;
  users: any;
  pageViews: any;
  pagesSession: any;
  sessions: any;
  avgTimeOnPage: any;
  bounceRate: any;
  newSessions: any;
  totalPageViews : any;
  visits:number;
  uniqueVisitors:number;
}
