import { Component, OnInit } from "@angular/core";
import { apiConfig } from "./../../config/global";
import { Router } from '@angular/router';
import { SecurityService } from "../common/services/authentication/security.service";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.css"]
})
export class MainComponent implements OnInit {
  constructor(
    private securityService:SecurityService,
    private router: Router
  ) {
    router.errorHandler = (error) => {
      if (error) {
        // this code will execute if there no route exists for the current url
        //router.navigate(['/DashboardMain']);
        if (JSON.parse(sessionStorage.getItem("id_token"))!==null) {
          this.router.navigate(["/dashboard/audience"]);
        }
        else {
          this.router.navigate(["/login"]);
        }
      }
    }
  }

  ngOnInit() {
    //this.globalService.getParam('login');
    //console.log(this.securityService.userData);

  }
  public  logout() {
    this.securityService.logout();
  }
}
