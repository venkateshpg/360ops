import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule, Http, RequestOptions } from "@angular/http";
import { ChartModule, HIGHCHARTS_MODULES } from "angular-highcharts";
import { Daterangepicker, DaterangepickerConfig } from "ng2-daterangepicker";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';


import {
  MainComponent,
  DashboardComponent,
  AudienceComponent,
  PerformanceComponent,
  FinanceComponent,
  LoginComponent,
  RegisterComponent,
  ForgotComponent
} from "./app.component";

import {
  SecurityService,
  RouteGuardService,
  GlobalService,
  HttpClient
} from "./app.services";

import { routing, appRoutingProviders } from "./app.routes";

@NgModule({
  declarations: [
    MainComponent,
    DashboardComponent,
    AudienceComponent,
    PerformanceComponent,
    FinanceComponent,
    LoginComponent,
    RegisterComponent,
    ForgotComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    routing,
    ChartModule,
    Daterangepicker,
    FormsModule,
    ReactiveFormsModule,
    MultiselectDropdownModule,
    NgbModule.forRoot(),
  ],
  providers: [
    appRoutingProviders,
    SecurityService,
    RouteGuardService,
    GlobalService,
    HttpClient
  ],

  bootstrap: [MainComponent]
})
export class AppModule { }
