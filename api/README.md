# README #

# This repository will be used for hosting Travel Sync Admin 3 using Node.js and Angular.

### What is this repository for? ###

* Development of 360OPS using Node.js & AngularJs
* 0.1

### How do I get set up? ###

##web
* open web folder in an editor 
* open terminal and navigate to web folder
* install node packages using "npm i"
* open browser http://localhost:4200/web

##api
* open api folder in an editor 
* open terminal and navigate to api folder
* install node packages using "npm i"
* open .env file in ./environment folder to configure database credentials and mail server settings
