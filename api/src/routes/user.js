var mail = require("../assets/js/Mail");
var jwt = require("jsonwebtoken");
var crypto = require("../helpers/crypto");

module.exports = function (models, router) {
  return router
    .Router().post("/login", function (req, res) {
      let user = req.body;
      models.aspnetusers
        .findOne({
          attributes: ['id', 'userName', 'firstName', 'lastName', 'isActive', 'roleId'],
          where: {
            userName: user.userName,
            passwordHash: user.passwordHash
          }
        })
        .then(function (user) {
          if (null == user) {
            res.status(401).send({ message: "invalid UserName or Password" })
          } else {
            var userModel = {
              id: user.id,
              userName: user.userName,
              firstName: user.firstName,
              lastName: user.lastName,
              roleId: user.roleId
            };
            userModel.token = jwt.sign(userModel, 'ilovemean');
            delete user.passwordHash;
            res
              .json(userModel);
          }
        })
        .catch(function (error) {
          res.boom.notAcceptable(error);
        });
    })

    .post("", function (req, res) {
      var user = req.body.user;
      models
        .aspnetusers
        .create(user)
        .then(function (response) {
          res
            .json(response);
        })
        .catch(function (error) {
          res
            .boom
            .badData(error);
        });
    })
    .get("/forgot/:un", function (req, res) {
      var un = req.params.un;
      models.aspnetusers
        .findOne({
          where: {
            userName: un
          }
        })
        .then(function (data) {
          var message =
            "Hi,\n\nYour password for 360ops login is: " +
            data.passwordHash +
            " . Keep it safe.\n\nThank & Regards,\n360ops Helpdesk";
          mail.sendMail(data.email, "Your Password", message, function (
            response
          ) {
            if (response == 1) {
              resp = {
                Status: 200,
                Message: "Password is sent successfully"
              };
              //callback(response);
            } else {
              resp = {
                Status: 400,
                Message: "Error in network, try again"
              };
              //callback(response);
            }
          });
          res
            .json(data);
        })
        .catch(function (error) {
          res
            .boom
            .notAcceptable(error);
        });
    })

    ;
};
