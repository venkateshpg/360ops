const moment = require("moment");
const Op = require("sequelize").Op;

module.exports = function (models, router) {
  return (
    router
      .Router()
      //Performance Model 
      .post("/staginganalytics", function (req, res) {
        var startDate = moment(req.body.startDate).format("YYYY-MM-DD");
        var endDate = moment(req.body.endDate).format("YYYY-MM-DD");
        models.Staging_Analytics
          .findAll({
            required: true,
            include: [
              {
                model: models.DataPublishers,
                as: "StagingDataPublishers",
                required: true,
                attributes: ["dataSourceName"]
              }
            ],
            attributes: [
              "dataSource",
              "pageViews",
              "pagesSession",
              [
                models.sequelize.literal("(CASE WHEN Click_Through_Rate IS NULL OR Click_Through_Rate = 0 THEN ROUND(Clicks/Impressions, 4) ELSE 0 END)"),
                "clickThroughRate"
              ], [
                models.sequelize.literal("ROUND(Avg_Time_on_Page)"),
                "avgTimeOnPage"
              ], [
                models.sequelize.literal("coalesce (Viewability, 0)"),
                "viewability"
              ],
              "bounceRate",
              "date"
            ],
            where: {
              date: {
                [Op.between]: [startDate, endDate]
              },
              'dataSource': {
                [Op.in]: req.body.datasource.toString().replace(/\//g, ",").split(",")
              }
            },
            group: ["date"],
            order: [
              ["date"] // Sorts by COLUMN_NAME_EXAMPLE in ascending order
            ],
            raw: true
          })
          .then(function (data) {
            res.json(data);
          })
          .catch(function (error) {
            res.boom.notAcceptable(error);
          });
      })
      .get("/dataPublishers/:role", function (req, res) {
        let role = req.params.role;
        models.DataPublishers
          .findAll({
            include: [
              {
                model: models.aspnetuserroles,
                as: "userroles",
                required: true,
                attributes: [],
                where: {
                  roleId: role
                }
              }
            ],
            attributes: [
              ["dataSourceId", "id"],
              ["dataSourceName", "name"],
            ],
            order: [
              ["dataSourceName"]
            ]
          })
          .then(function (data) {
            res
              .json(data);
          })
          .catch(function (error) {
            res
              .boom
              .notAcceptable(error);
          });
      })
      //Financial Analytics
      .post("/earninganalytics", function (req, res) {
        var startDate = moment(req.body.startDate).format("YYYY-MM-DD");
        var endDate = moment(req.body.endDate).format("YYYY-MM-DD");
        models.Staging_Analytics
          .findAll({
            attributes: [
              "DataSource",
              "date",
              [models.sequelize.literal(" CONVERT(REPLACE (coalesce (Revenue, 0), '$',''),DECIMAL)"),
                "revenue"],
              [models.sequelize.literal("CONVERT(REPLACE (coalesce (eCPM, 0), '$',''),DECIMAL)"),
                "ecpm"],
              [models.sequelize.literal("coalesce (Requests, 0)"),
                "requests"],
              [models.sequelize.literal("coalesce (Fill_Rate, 0)"),
                "fillRate"],
              [models.sequelize.literal("CONVERT(REPLACE (coalesce (Revenue_per_Impressions, 0), '$',''),DECIMAL)"),
                "revenuePerImpressions"],
              [models.sequelize.literal("coalesce (Impressions, 0)"),
                "impressions"]
            ],
            where: {
              date: {
                [Op.between]: [startDate, endDate]
              },
              'dataSource': {
                [Op.in]: req.body.datasource.toString().replace(/\//g, ",").split(",")
              }
              //dataSource: 4
            },
            group: ["date"],
            order: [["date"]]
          })
          .then(function (data) {
            res.json(data);
          })
          .catch(function (error) {
            res.boom.notAcceptable(error);
          });
      })
      //Total Earning by data source
      .post("/totalearnings", function (req, res) {
        var startDate = moment(req.body.startDate).format("YYYY-MM-DD");
        var endDate = moment(req.body.endDate).format("YYYY-MM-DD");
        models.Staging_Analytics
          .findAll({
            required: true,
            include: [
              {
                model: models.DataPublishers,
                as: "StagingDataPublishers",
                required: true,
                attributes: ["dataSourceName"]
              }
            ],
            attributes: [
              "dataSource",
              [
                models.sequelize.literal("SUM(coalesce (Revenue, 0))"),
                "revenue"
              ]
            ],
            where: {
              date: {
                [Op.between]: [startDate, endDate]
              }
            },
            group: ["DataSource"],
            order: [["date"]],
            raw: true
          })
          .then(function (data) {
            res.json(data);
          })
          .catch(function (error) {
            res.boom.notAcceptable(error);
          });
      })
      // Audience Report
      .get("/:startDate/:endDate", function (req, res) {
        models.GoogleAnalytics
          .findAll({
            attributes: [
              "dataSource",
              "date",
              "pagesSession",
              "sessions",
              [models.sequelize.literal("REPLACE(Users, ',', '')"),
                "users"],
              [models.sequelize.literal("REPLACE(Pageviews, ',', '')"),
                "pageViews"],
              [
                models.sequelize.literal("SEC_TO_TIME(AVG(Avg__Time_on_Page))"),
                "avgTimeOnPage"
              ],
              [
                models.sequelize.literal("AVG(Bounce_Rate)"),
                "bounceRate"
              ],
              "newSessions"
            ],
            where: {
              date: {
                [Op.between]: [req.params.startDate, req.params.endDate]
              }
            },
            group: ["date"],
            order: [
              ["date"] // Sorts by COLUMN_NAME_EXAMPLE in ascending order
            ]
          })
          .then(function (data) {
            res.json(data);
          })
          .catch(function (error) {
            res.boom.notAcceptable(error);
          });
      })

  );
};
