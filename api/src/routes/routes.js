var models = require("../models/index.js")
authenticate = require('../middleware/authenticate.js');

module.exports = function (app, router, routeStart) {
  app.use(routeStart + '/user', require("./user")(models, router));
  app.use(routeStart + '/reports', authenticate, require("./reports")(models, router));

  app.use("/", function (req, res) {
    res.send("TS http://" + req.headers.host + "/api");
  });
};
