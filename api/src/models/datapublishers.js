"use strict";
module.exports = function(sequelize, DataTypes) {
  var DataPublishers = sequelize.define(
    "DataPublishers",
    {
      dataSourceId: {
        field: "DataSourceId",
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      dataSourceName: {
        field: "DataSourceName",
        type: DataTypes.STRING
      }
    },
    {
      timestamps: false
    }
  );

  return DataPublishers;
};
