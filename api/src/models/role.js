"use strict";
module.exports = function(sequelize, DataTypes) {
  var role = sequelize.define(
    "role",
    {
      id: {
        field: "Id",
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        field: "Name",
        type: DataTypes.STRING
      }
    },
    {
      timestamps: false
    }
  );

  return role;
};
