"use strict";
module.exports = function(sequelize, DataTypes) {
  var aspnetuserroles = sequelize.define(
    "aspnetuserroles",
    {
      roleId: {
        field: "RoleId",
        type: DataTypes.INTEGER
      },
      dataSource: {
        field: "DataSource",
        type: DataTypes.STRING
      }
    },
    {
      timestamps: false
    }
  );

  return aspnetuserroles;
};
