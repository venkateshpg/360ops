"use strict";
module.exports = function(sequelize, DataTypes) {
  var Staging_Analytics = sequelize.define(
    "Staging_Analytics",
    {
      dataSource: {
        field: "DataSource",
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      date: {
        field: "Date",
        type: DataTypes.STRING
      },
      demandPartner: {
        field: "Demand_Partner",
        type: DataTypes.STRING,
        allowNull: true
      },
      impressions: {
        field: "Impressions",
        type: DataTypes.STRING
      },
      revenue: {
        field: "Revenue",
        type: DataTypes.STRING
      },
      ecpm: {
        field: "eCPM",
        type: DataTypes.STRING
      },
      placementName: {
        field: "Placement_Name",
        type: DataTypes.STRING
      },
      placementId: {
        field: "Placement_ID",
        type: DataTypes.INTEGER
      },
      requests: {
        field: "Requests",
        type: DataTypes.STRING
      },
      advertiser: {
        field: "Advertiser",
        type: DataTypes.STRING
      },
      advertiserId: {
        field: "Advertiser_ID",
        type: DataTypes.INTEGER
      },
      totalCpmCpcAndCpdRevenue: {
        field: "Total_CPM_CPC_and_CPD_revenue",
        type: DataTypes.STRING
      },
      adRequests: {
        field: "Ad_Requests",
        type: DataTypes.INTEGER
      },
      matchedAdRequests: {
        field: "Matched_Ad_Requests",
        type: DataTypes.INTEGER
      },
      fillRate: {
        field: "Fill_Rate",
        type: DataTypes.STRING
      },
      estimatedNetEarnings: {
        field: "Estimated_Net_Earnings",
        type: DataTypes.INTEGER
      },
      clicks: {
        field: "Clicks",
        type: DataTypes.INTEGER
      },
      clickThroughRate: {
        field: "Click_Through_Rate",
        type: DataTypes.STRING
      },
      revenuePerImpressions: {
        field: "Revenue_per_Impressions",
        type: DataTypes.STRING
      },
      adunit: {
        field: "Ad_unit",
        type: DataTypes.STRING
      },
      ioCpm: {
        field: "IO_CPM",
        type: DataTypes.STRING
      },
      pageViews: {
        field: "Pageviews",
        type: DataTypes.STRING
      },
      viewability: {
        field: "Viewability",
        type: DataTypes.STRING
      },
      engagement: {
        field: "Engagement",
        type: DataTypes.STRING
      },
      unitsPerPage: {
        field: "Units_Per_Page",
        type: DataTypes.STRING
      },
      users: {
        field: "Users",
        type: DataTypes.INTEGER
      },
      pagesSession: {
        field: "Pages_Session",
        type: DataTypes.INTEGER
      },
      sessions: {
        field: "Sessions",
        type: DataTypes.INTEGER
      },
      avgTimeOnPage: {
        field: "Avg_Time_on_Page",
        type: DataTypes.INTEGER
      },
      bounceRate: {
        field: "Bounce_Rate",
        type: DataTypes.INTEGER
      },
      newSessions: {
        field: "New_Sessions",
        type: DataTypes.INTEGER
      },
      id: {
        field: "Id",
        type: DataTypes.INTEGER
      },
      name: {
        field: "Name",
        type: DataTypes.STRING
      },
      adsReceived: {
        field: "Ads_Received",
        type: DataTypes.INTEGER
      },
      impressionFillRate: {
        field: "Impression_Fill_Rate",
        type: DataTypes.STRING
      },
      auctions: {
        field: "Auctions",
        type: DataTypes.INTEGER
      },
      adResponses: {
        field: "Ad_Responses",
        type: DataTypes.INTEGER
      },
      rCpm: {
        field: "rCPM",
        type: DataTypes.INTEGER
      },
      rpm: {
        field: "rpm",
        type: DataTypes.STRING
      }
    },
    {
      timestamps: false
    }
  );

  return Staging_Analytics;
};
