"use strict";
module.exports = function(sequelize, DataTypes) {
  var GoogleAnalytics = sequelize.define(
    "GoogleAnalytics",
    {
      dataSource: {
        field: "DataSource",
        type: DataTypes.INTEGER
      },
      date: {
        field: "Date",
        type: DataTypes.STRING
      },
      users: {
        field: "Users",
        type: DataTypes.STRING
      },
      pageViews: {
        field: "Pageviews",
        type: DataTypes.STRING
      },
      pagesSession: {
        field: "Pages___Session",
        type: DataTypes.STRING
      },
      sessions: {
        field: "Sessions",
        type: DataTypes.STRING
      },
      avgTimeOnPage: {
        field: "Avg__Time_on_Page",
        type: DataTypes.STRING
      },
      bounceRate: {
        field: "Bounce_Rate",
        type: DataTypes.STRING
      },
      newSessions: {
        field: "__New_Sessions",
        type: DataTypes.STRING
      }
    },
    {
      timestamps: false
    }
  );

  return GoogleAnalytics;
};
