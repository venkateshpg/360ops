"use strict";
module.exports = function(sequelize, DataTypes) {
  var aspnetusers = sequelize.define(
    "aspnetusers",
    {
      id: {
        field: "Id",
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      userName: {
        field: "UserName",
        type: DataTypes.STRING
      },
      passwordHash: {
        field: "PasswordHash",
        type: DataTypes.STRING,
        allowNull: true
      },
      firstName: {
        field: "FirstName",
        type: DataTypes.STRING,
         allowNull: true
     },
      lastName: {
        field: "LastName",
        type: DataTypes.STRING,
         allowNull: true
     },
      companyId: {
        field: "CompanyId",
        type: DataTypes.INTEGER,
         allowNull: true
     },
      isActive: {
        field: "IsActive",
        type: DataTypes.INTEGER,
         allowNull: true
     },
      roleId: {
        field: "RoleId",
        type: DataTypes.INTEGER,
         allowNull: true
     }
    },
    {
      timestamps: false
    }
  );

  return aspnetusers;
};
