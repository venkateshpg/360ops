var nodemailer = require('nodemailer');

var method = {
    sendMail: function (TO, SUBJECT, MESSAGE, callback) {
        var transporter = nodemailer.createTransport({
            service: process.env.EMAIL_SERVICE,
            auth: {
                user: process.env.EMAIL_USER,
                pass: process.env.EMAIL_PASSWORD
            }
        });

        var mailOptions = {
            from: '360Ops Helpdesk',
            to: TO,
            subject: SUBJECT,
            text: MESSAGE
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
                callback(0);
            } else {
                console.log('Email sent: ' + info.response);
                callback(1);
            }
        });
    }
}

module.exports = method;