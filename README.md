# 360OPS
## Softwares Needed
-NODEJS
-GIT BASH
-MYSQL
-VISUAL STUDIO CODE(EDITOR)

This repository will be used for hosting 360OPS using Node.js and Angular.

#What is this repository for?
Development of 360OPS using Node.js & AngularJs
0.1
##How do I get set up?

##get files from BITBUCKET server

git clone https://bitbucket.org/venkateshpg/360ops.git

#web
open web folder in an  Visual Studio Code Editor
open terminal and navigate to web folder
install angular cli "npm i -g @angular/cli --save"

*add angular cli path to system environment variables path 
for example : "set PATH=%PATH%;C:\Users\360ops\AppData\Roaming\npm;"

install node packages using "npm i"
open browser http://localhost:4200/web

#api
open api folder in an Visual Studio Code Editor
open terminal and navigate to api folder
install node packages using "npm i"
open .env file in ./environment folder to configure database credentials and mail server settings
launch the application using cmd "node server.js" or Press 'F5' key in keyboard to start debugger mode.
